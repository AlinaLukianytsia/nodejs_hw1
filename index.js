const express = require('express');
const morgan = require('morgan');
const fs = require('fs').promises;
const fsS = require('fs');
const bodyParser = require('body-parser');
const path = require('path');
const router = express.Router();
const app = express();
const port = 8080;
const regExpForFileExt = /[\w\d]\.(log|txt|json|yaml|xml|js)$/;
const pathToFiles = 'api/files';

app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

router.post('/', async (req, res) => {
    const {filename, content} = req.body

    if (!regExpForFileExt.test(filename)) {
        return res.status(400).json({message: `File type is invalid!`})
    }

    if (!content || !content.trim()) {
        return res.status(400).json({message: `Please specify 'content' parameter`})
    }

    if (!fsS.existsSync(`./${pathToFiles}`)){
      await fsS.mkdirSync(`./api`,null);
      await fsS.mkdirSync(`./api/files/`,null);
    }

    try {
        await fs.writeFile(`${pathToFiles}/${filename}`, `${content}`, 'utf-8')
        res.status(200).json({message: 'File created successfully'})
    } catch {
        res.status(500).json({message: 'Server error'})
    }
})

router.get('/', async (req, res) => {
    try {
        const dirData = await fs.readdir(`${pathToFiles}`)
        res.status(200).json({message: 'Success', files: dirData})
    } catch {
        res.status(500).json({message: 'Server error'})
    }
})

router.get('/:filename', async (req, res) => {
    const filename = req.params.filename;
    const dirData = await fs.readdir(`${pathToFiles}`)

    if (!dirData.includes(filename)) {
        return res.status(400).json({message: `No file with '${filename}' filename found`})
    }

    try {
        const fileExt = path.extname(filename).slice(1)
        const fileContent = await fs.readFile(`${pathToFiles}/${filename}`)
        const fileUploadedInfo = await fs.stat(`${pathToFiles}/${filename}`)
        const fileUploadedDate = fileUploadedInfo.birthtime

        res.status(200).json({
            message: 'Success',
            filename: `${filename}`,
            content: `${fileContent}`,
            extension: `${fileExt}`,
            uploadedDate: `${fileUploadedDate}`
        })
    } catch {
        res.status(500).json({message: 'Server error'})
    }
})

app.use(`/${pathToFiles}`, router);

app.listen(port)